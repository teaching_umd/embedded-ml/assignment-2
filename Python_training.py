# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 15:24:15 2021

@author: sshah389
"""

import os
import pathlib
from matplotlib import animation

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import scipy.io as sio
from matplotlib import pyplot 
from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras import layers
from keras.models import load_model
from tensorflow.keras import models
from tensorflow import keras
from sklearn.model_selection import train_test_split
import pandas as pd
from keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.metrics import mean_squared_error
import time
from scipy import stats
from keras.utils.np_utils import to_categorical   



   
train_data=sio.loadmat('./Dataset/MNIST_database_train.mat') 
image_train=train_data['images']
label_train=train_data['labels']
x_train, x_validate, y_train, y_validate = train_test_split(image_train,label_train, test_size = 0.20,shuffle=False)


y_train=to_categorical(y_train, num_classes=10)
y_validate=to_categorical(y_validate, num_classes=10)

model = tf.keras.Sequential()
model.add(layers.Dense(10, activation='softmax',input_shape=(784,)))  
opt = keras.optimizers.SGD(learning_rate=0.01)
model.compile(optimizer=opt,  loss=tf.keras.losses.CategoricalCrossentropy ())
EPOCHS =1000
callbacks = [EarlyStopping(monitor='val_loss', patience=8), ModelCheckpoint(filepath='./best_model.h5', monitor='val_loss', save_best_only=True)]# uses validation set to stop training when it start overfitting
history = model.fit(x_train,y_train,validation_data=(x_validate,y_validate),callbacks=callbacks,epochs=EPOCHS,batch_size=32, verbose=2, shuffle=True)
model=keras.models.load_model('./best_model.h5')
metrics = history.history
plt.plot(history.epoch, metrics['loss'], metrics['val_loss'])
plt.legend(['loss', 'val_loss'])
plt.show()



   
test_data=sio.loadmat('./Dataset/MNIST_database_test.mat') 
image_test=test_data['images']
label_test=np.reshape(test_data['labels'],[-1,])
label_predict=np.argmax(model.predict(image_test,batch_size=1),axis=0)
accuracy=0
for i in range(0,10000):
    accuracy+= (label_predict[i] == label_test[i])
print('Percentage Accuracy')
print(accuracy*100/10000)

model.summary()

Layer1_weights=model.get_weights()[0]
Layer1_bias=model.get_weights()[1]


Model_para_inputs = {"Layer1_weights":Layer1_weights,"Layer1_bias":Layer1_bias,"image_test":image_test,"label_test":label_test,"label_predict":label_predict}
sio.savemat('./Model_para_inputs.mat',Model_para_inputs)