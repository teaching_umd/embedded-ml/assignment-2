/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Ultra(TM) in wire load mode
// Version   : Q-2019.12-SP4
// Date      : Mon Nov 15 18:43:04 2021
/////////////////////////////////////////////////////////////


module qmac ( i_multiplicand, i_multiplier, clk, enable, out_adder, vdd, gnd
 );
  input [3:0] i_multiplicand;
  input [3:0] i_multiplier;
  output [3:0] out_adder;
  input clk, enable;
  input vdd;
  input gnd;
  wire   n158, n159, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57,
         n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n70, n72, n73,
         n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
         n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157;
  wire   [3:0] out_mult1;
  wire   [3:0] o_mac;

  DFFSR out_mult1_reg_3_ ( .D(n56), .CLK(clk), .R(enable), .S(1'b1), .Q(
        out_mult1[3]), .vdd(vdd), .gnd(gnd) );
  DFFSR out_mult1_reg_2_ ( .D(n57), .CLK(clk), .R(enable), .S(1'b1), .Q(
        out_mult1[2]), .vdd(vdd), .gnd(gnd) );
  DFFSR out_mult1_reg_1_ ( .D(n58), .CLK(clk), .R(enable), .S(1'b1), .Q(
        out_mult1[1]), .vdd(vdd), .gnd(gnd) );
  DFFSR out_mult1_reg_0_ ( .D(n51), .CLK(clk), .R(enable), .S(1'b1), .Q(
        out_mult1[0]), .vdd(vdd), .gnd(gnd) );
  DFFSR o_mac_reg_0_ ( .D(n59), .CLK(clk), .R(enable), .S(1'b1), .Q(o_mac[0]), .vdd(vdd), .gnd(gnd) );
  DFFSR o_mac_reg_3_ ( .D(n60), .CLK(clk), .R(enable), .S(1'b1), .Q(o_mac[3]), .vdd(vdd), .gnd(gnd) );
  DFFSR o_mac_reg_1_ ( .D(n61), .CLK(clk), .R(enable), .S(1'b1), .Q(o_mac[1]), .vdd(vdd), .gnd(gnd) );
  DFFSR o_mac_reg_2_ ( .D(n89), .CLK(clk), .R(enable), .S(1'b1), .Q(o_mac[2]), .vdd(vdd), .gnd(gnd) );
  AND2X1 U60 ( .A(n84), .B(n148), .Y(n142), .vdd(vdd), .gnd(gnd) );
  OR2X1 U61 ( .A(n86), .B(n103), .Y(n113), .vdd(vdd), .gnd(gnd) );
  AND2X1 U62 ( .A(n77), .B(n113), .Y(n115), .vdd(vdd), .gnd(gnd) );
  AND2X1 U63 ( .A(n72), .B(n126), .Y(n139), .vdd(vdd), .gnd(gnd) );
  OR2X1 U64 ( .A(n96), .B(n62), .Y(n119), .vdd(vdd), .gnd(gnd) );
  AND2X1 U65 ( .A(n73), .B(n94), .Y(n137), .vdd(vdd), .gnd(gnd) );
  OR2X1 U66 ( .A(n79), .B(n96), .Y(n104), .vdd(vdd), .gnd(gnd) );
  AND2X1 U67 ( .A(n83), .B(n74), .Y(n151), .vdd(vdd), .gnd(gnd) );
  BUFX2 U68 ( .A(n123), .Y(n55), .vdd(vdd), .gnd(gnd) );
  BUFX2 U69 ( .A(n54), .Y(n56), .vdd(vdd), .gnd(gnd) );
  BUFX2 U70 ( .A(n53), .Y(n57), .vdd(vdd), .gnd(gnd) );
  BUFX2 U71 ( .A(n52), .Y(n58), .vdd(vdd), .gnd(gnd) );
  BUFX2 U72 ( .A(n50), .Y(n59), .vdd(vdd), .gnd(gnd) );
  BUFX2 U73 ( .A(n49), .Y(n60), .vdd(vdd), .gnd(gnd) );
  BUFX2 U74 ( .A(n48), .Y(n61), .vdd(vdd), .gnd(gnd) );
  INVX1 U75 ( .A(n101), .Y(n62), .vdd(vdd), .gnd(gnd) );
  AND2X1 U76 ( .A(i_multiplicand[0]), .B(i_multiplier[0]), .Y(n101), .vdd(vdd), .gnd(gnd) );
  BUFX2 U77 ( .A(n154), .Y(n63), .vdd(vdd), .gnd(gnd) );
  INVX1 U78 ( .A(n109), .Y(n64), .vdd(vdd), .gnd(gnd) );
  OR2X1 U79 ( .A(n107), .B(n117), .Y(n109), .vdd(vdd), .gnd(gnd) );
  INVX1 U80 ( .A(n143), .Y(n65), .vdd(vdd), .gnd(gnd) );
  OR2X1 U81 ( .A(n97), .B(n141), .Y(n143), .vdd(vdd), .gnd(gnd) );
  INVX1 U82 ( .A(n146), .Y(n66), .vdd(vdd), .gnd(gnd) );
  AND2X1 U83 ( .A(n84), .B(n85), .Y(n146), .vdd(vdd), .gnd(gnd) );
  BUFX2 U84 ( .A(n150), .Y(n67), .vdd(vdd), .gnd(gnd) );
  INVX1 U85 ( .A(out_adder[3]), .Y(n68), .vdd(vdd), .gnd(gnd) );
  BUFX2 U86 ( .A(n158), .Y(out_adder[3]), .vdd(vdd), .gnd(gnd) );
  INVX1 U87 ( .A(out_adder[2]), .Y(n70), .vdd(vdd), .gnd(gnd) );
  BUFX2 U88 ( .A(n159), .Y(out_adder[2]), .vdd(vdd), .gnd(gnd) );
  BUFX2 U89 ( .A(o_mac[2]), .Y(n72), .vdd(vdd), .gnd(gnd) );
  INVX1 U90 ( .A(n128), .Y(n73), .vdd(vdd), .gnd(gnd) );
  AND2X1 U91 ( .A(n85), .B(n149), .Y(n128), .vdd(vdd), .gnd(gnd) );
  INVX1 U92 ( .A(n127), .Y(n74), .vdd(vdd), .gnd(gnd) );
  AND2X1 U93 ( .A(n75), .B(n157), .Y(n127), .vdd(vdd), .gnd(gnd) );
  BUFX2 U94 ( .A(out_mult1[2]), .Y(n75), .vdd(vdd), .gnd(gnd) );
  INVX1 U95 ( .A(n119), .Y(n76), .vdd(vdd), .gnd(gnd) );
  INVX1 U96 ( .A(n114), .Y(n77), .vdd(vdd), .gnd(gnd) );
  AND2X1 U97 ( .A(n86), .B(n103), .Y(n114), .vdd(vdd), .gnd(gnd) );
  AND2X1 U98 ( .A(n97), .B(n141), .Y(n125), .vdd(vdd), .gnd(gnd) );
  INVX1 U99 ( .A(n125), .Y(n78), .vdd(vdd), .gnd(gnd) );
  INVX1 U100 ( .A(n100), .Y(n79), .vdd(vdd), .gnd(gnd) );
  AND2X1 U101 ( .A(i_multiplier[2]), .B(i_multiplicand[0]), .Y(n100), .vdd(vdd), .gnd(gnd) );
  INVX1 U102 ( .A(n147), .Y(n80), .vdd(vdd), .gnd(gnd) );
  AND2X1 U103 ( .A(n87), .B(n97), .Y(n147), .vdd(vdd), .gnd(gnd) );
  BUFX2 U104 ( .A(n121), .Y(n81), .vdd(vdd), .gnd(gnd) );
  INVX1 U105 ( .A(n137), .Y(n82), .vdd(vdd), .gnd(gnd) );
  INVX1 U106 ( .A(n139), .Y(n83), .vdd(vdd), .gnd(gnd) );
  BUFX2 U107 ( .A(out_mult1[1]), .Y(n84), .vdd(vdd), .gnd(gnd) );
  BUFX2 U108 ( .A(o_mac[1]), .Y(n85), .vdd(vdd), .gnd(gnd) );
  INVX1 U109 ( .A(n104), .Y(n86), .vdd(vdd), .gnd(gnd) );
  BUFX2 U110 ( .A(o_mac[0]), .Y(n87), .vdd(vdd), .gnd(gnd) );
  BUFX2 U111 ( .A(out_mult1[3]), .Y(n88), .vdd(vdd), .gnd(gnd) );
  BUFX2 U112 ( .A(n47), .Y(n89), .vdd(vdd), .gnd(gnd) );
  INVX1 U113 ( .A(n115), .Y(n90), .vdd(vdd), .gnd(gnd) );
  AND2X1 U114 ( .A(i_multiplier[2]), .B(i_multiplicand[2]), .Y(n111), .vdd(vdd), .gnd(gnd) );
  INVX1 U115 ( .A(n111), .Y(n91), .vdd(vdd), .gnd(gnd) );
  BUFX2 U116 ( .A(o_mac[3]), .Y(n92), .vdd(vdd), .gnd(gnd) );
  BUFX2 U117 ( .A(n108), .Y(n93), .vdd(vdd), .gnd(gnd) );
  INVX1 U118 ( .A(n142), .Y(n94), .vdd(vdd), .gnd(gnd) );
  INVX1 U119 ( .A(n151), .Y(n95), .vdd(vdd), .gnd(gnd) );
  AND2X1 U120 ( .A(i_multiplier[1]), .B(i_multiplicand[1]), .Y(n102), .vdd(vdd), .gnd(gnd) );
  INVX1 U121 ( .A(n102), .Y(n96), .vdd(vdd), .gnd(gnd) );
  BUFX2 U122 ( .A(out_mult1[0]), .Y(n97), .vdd(vdd), .gnd(gnd) );
  INVX1 U123 ( .A(i_multiplier[3]), .Y(n98), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U124 ( .B(i_multiplier[3]), .A(n98), .S(i_multiplicand[3]), .Y(n99), .vdd(vdd), .gnd(gnd) );
  INVX1 U125 ( .A(n88), .Y(n133), .vdd(vdd), .gnd(gnd) );
  INVX1 U126 ( .A(enable), .Y(n156), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U127 ( .A(enable), .B(n99), .C(n133), .D(n156), .Y(n54), .vdd(vdd), .gnd(gnd) );
  AOI21X1 U128 ( .A(n79), .B(n96), .C(n86), .Y(n121), .vdd(vdd), .gnd(gnd)
         );
  AND2X1 U129 ( .A(i_multiplicand[2]), .B(i_multiplier[0]), .Y(n120), .vdd(vdd), .gnd(gnd) );
  INVX1 U130 ( .A(n113), .Y(n107), .vdd(vdd), .gnd(gnd) );
  AND2X1 U131 ( .A(i_multiplier[2]), .B(i_multiplicand[1]), .Y(n106), .vdd(vdd), .gnd(gnd) );
  AND2X1 U132 ( .A(i_multiplicand[2]), .B(i_multiplier[1]), .Y(n105), .vdd(vdd), .gnd(gnd) );
  NAND3X1 U133 ( .A(i_multiplier[2]), .B(i_multiplicand[1]), .C(n105), .Y(n108), .vdd(vdd), .gnd(gnd) );
  OAI21X1 U134 ( .A(n106), .B(n105), .C(n93), .Y(n117), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U135 ( .B(n77), .A(n64), .S(n93), .Y(n110), .vdd(vdd), .gnd(gnd)
         );
  XNOR2X1 U136 ( .A(n91), .B(n110), .Y(n112), .vdd(vdd), .gnd(gnd) );
  INVX1 U137 ( .A(n75), .Y(n126), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U138 ( .A(enable), .B(n112), .C(n126), .D(n156), .Y(n53), .vdd(vdd), .gnd(gnd) );
  INVX1 U139 ( .A(n117), .Y(n116), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U140 ( .B(n117), .A(n116), .S(n90), .Y(n118), .vdd(vdd), .gnd(gnd) );
  INVX1 U141 ( .A(n84), .Y(n149), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U142 ( .A(enable), .B(n118), .C(n149), .D(n156), .Y(n52), .vdd(vdd), .gnd(gnd) );
  FAX1 U143 ( .A(n81), .B(n120), .C(n76), .YC(n103), .YS(n122), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U144 ( .A(enable), .B(n122), .C(n97), .D(n156), .Y(n123), .vdd(vdd), .gnd(gnd) );
  INVX1 U145 ( .A(n55), .Y(n51), .vdd(vdd), .gnd(gnd) );
  INVX1 U146 ( .A(n87), .Y(n141), .vdd(vdd), .gnd(gnd) );
  OAI21X1 U147 ( .A(n97), .B(n141), .C(n78), .Y(out_adder[0]), .vdd(vdd), .gnd(gnd) );
  INVX1 U148 ( .A(out_adder[0]), .Y(n124), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U149 ( .A(enable), .B(n124), .C(n141), .D(n156), .Y(n50), .vdd(vdd), .gnd(gnd) );
  INVX1 U150 ( .A(n92), .Y(n134), .vdd(vdd), .gnd(gnd) );
  INVX1 U151 ( .A(n72), .Y(n157), .vdd(vdd), .gnd(gnd) );
  INVX1 U152 ( .A(n85), .Y(n148), .vdd(vdd), .gnd(gnd) );
  OAI21X1 U153 ( .A(n128), .B(n78), .C(n94), .Y(n140), .vdd(vdd), .gnd(gnd) );
  OAI21X1 U154 ( .A(n127), .B(n140), .C(n83), .Y(n132), .vdd(vdd), .gnd(gnd) );
  OR2X1 U155 ( .A(n82), .B(out_adder[0]), .Y(n129), .vdd(vdd), .gnd(gnd)
         );
  OAI21X1 U156 ( .A(n95), .B(n129), .C(n132), .Y(n130), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U157 ( .A(n134), .B(n132), .C(n133), .D(n130), .Y(n158), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U158 ( .A(enable), .B(n68), .C(n134), .D(n156), .Y(n49), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U159 ( .B(n132), .A(n87), .S(n97), .Y(n131), .vdd(vdd), .gnd(gnd)
         );
  OAI21X1 U160 ( .A(n87), .B(n132), .C(n131), .Y(n135), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U161 ( .B(n92), .A(n134), .S(n133), .Y(n155), .vdd(vdd), .gnd(gnd) );
  INVX1 U162 ( .A(n155), .Y(n152), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U163 ( .B(n135), .A(n80), .S(n152), .Y(n136), .vdd(vdd), .gnd(gnd) );
  HAX1 U164 ( .A(n82), .B(n136), .YS(out_adder[1]), .vdd(vdd), .gnd(gnd)
         );
  INVX1 U165 ( .A(out_adder[1]), .Y(n138), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U166 ( .A(enable), .B(n138), .C(n148), .D(n156), .Y(n48), .vdd(vdd), .gnd(gnd) );
  INVX1 U168 ( .A(n140), .Y(n145), .vdd(vdd), .gnd(gnd) );
  OAI21X1 U169 ( .A(n128), .B(n65), .C(n94), .Y(n144), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U170 ( .A(n139), .B(n145), .C(n127), .D(n144), .Y(n154), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U171 ( .A(n149), .B(n148), .C(n80), .D(n66), .Y(n150), .vdd(vdd), .gnd(gnd) );
  MUX2X1 U172 ( .B(n95), .A(n151), .S(n67), .Y(n153), .vdd(vdd), .gnd(gnd)
         );
  AOI22X1 U173 ( .A(n155), .B(n63), .C(n153), .D(n152), .Y(n159), .vdd(vdd), .gnd(gnd) );
  AOI22X1 U174 ( .A(enable), .B(n70), .C(n157), .D(n156), .Y(n47), .vdd(vdd), .gnd(gnd) );
endmodule

