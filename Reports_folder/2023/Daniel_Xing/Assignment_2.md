# Assignment 2 Report

## Overview

For the MAC unit, I used the following precisions:
* 16 bit: Q =11, R = 4
* 8 bit: Q = 5, R = 2
* 4 bit: Q = 2, R = 1

I chose the precisions so that they roughly followed the original 16 bit fixed point ratio. If I had more time, I would have tried experimenting with different ratios for a fixed number of bits and see how the accuracy would've changed.

The `qmac` Verilog module was synthesized at varying precisions using Synopsys Design Compiler with the provided synthesis script, then imported into Cadence Virtuoso. The MAC unit was simulated using ADE using a Verilog-A module as a testbench harness. Simulations and calculations were done using Cadence ADE. 

Getting the project to run took some time but I was able to get most things to work. Most of the time was spent getting used to all of the tools and figuring out what buttons to push, options to tick, etc. 

## Simulation results

![tr8](/Users/dxing97/Documents/Courses/2021Fall/ENEE719D/assignment-2/Reports_folder/Daniel_Xing/tr8.png)![tr4](/Users/dxing97/Documents/Courses/2021Fall/ENEE719D/assignment-2/Reports_folder/Daniel_Xing/tr4.png)

### Average power

Average power was calculated by finding average current and multiplying by 1V. 

* 16 bit: unknown (simulation didn't return values)
* 8 bit: 2.008 mW average power
* 4 bit: 66.3 uW average power

Average power for the 4 bit MAC unit was significantly less than the 8 bit MAC

### Total energy

Total energy was calculated by integrating current over the entire simulation and multiplying by 1V.

* 16 bit: unknown (simulation didn't return values)
* 8 bit: 235.0 nJ
* 4 bit: 77.4798 nJ

Total energy is saved is not quite as impressive, likely due to leakage.

### Accuracy

Accuracies were not that good when quantized inputs were used, likely due to quantization errors and possibly an error in modeling. Keeping the magnitude as large as possible while sacrificing some precision may help improve these results. 

* 16 bit:
  * expected value: 0.2842
  * simulated value: 0.9526
* 8 bit:
  * expected value: 0.2949
  * simulated value: 0.8438
* 4 bit:
  * expected value: -0.125
  * simulated value: 1.5
