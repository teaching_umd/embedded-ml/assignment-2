# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 16:50:51 2021

@author: sshah389
"""

#This file takes in the real value and quantize and creates a text file to input the values into cadence.
#Here the code is doing fixed point quantization


import scipy.io as sio
import numpy as np
import matplotlib.pyplot as pyplot


Q_SET = 2
R_SET = 1

def real_to_binary(weights,Q=11,R=4):
    d1=weights
    dim_array=d1.ndim
    if dim_array>1:
        [row_size,column_size]=d1.shape
    else:
        row_size=d1.shape
        d1=d1.reshape([row_size[0],1])
        [row_size,column_size]=d1.shape
    Q1=[["0"]]
    for j_loop in range(0,row_size-1):
        Q1.append(["0"])
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size-1):
                Q1[j_loop].append("0")
    integ_d1=np.floor(abs(d1))
    fract_d1=abs(d1)-integ_d1
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if d1[j_loop,i_loop]<0:
                Q1[j_loop][i_loop]='1'
            if d1[j_loop,i_loop]<=(2**R-1):
                Q1[j_loop][i_loop]+=np.binary_repr(int(integ_d1[j_loop,i_loop]),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int(fract_d1[j_loop,i_loop]*2**Q),width=Q)
            else:
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**R)-1),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**Q)-1),width=Q)              
    return Q1
            
                


def binary_to_real(weights_bin,Q=11,R=4):
    Q1=weights_bin
    row_size=len(Q1)
    column_size=len(Q1[0])
    if column_size>1:
        d1=np.zeros([row_size,column_size])
    else:
        d1=np.zeros([row_size,1])
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if Q1[j_loop][i_loop][0]=="0":
                sign_bit=1
            else:
                sign_bit=-1
            decimal_value=0    
            for decimcal_loop in range(Q+R,0,-1):
               if Q1[j_loop][i_loop][(Q+R)-decimcal_loop+1]=='1':
                   decimal_value=decimal_value+2**(decimcal_loop-Q-1)
               else:
                   decimal_value=decimal_value+0       
            d1[j_loop,i_loop]=sign_bit*decimal_value
    if column_size==1:
        d1=d1.reshape([row_size])        
    return d1

    
def real_to_decimal(weights,Q=11,R=4):
    d1=weights
    dim_array=d1.ndim
    if dim_array==1:
        row_size=d1.shape
        d1=d1.reshape([row_size[0],1])
    [row_size,column_size]=d1.shape
    decimal=np.zeros([row_size,column_size])
    Q1=[['0' for i in range(column_size)] for j in range(row_size)]
    integ_d1=np.floor(abs(d1))
    fract_d1=abs(d1)-integ_d1
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if d1[j_loop,i_loop]<0:
                Q1[j_loop][i_loop]='1'
            if d1[j_loop,i_loop]<=(2**R-1):
                Q1[j_loop][i_loop]+=np.binary_repr(int(integ_d1[j_loop,i_loop]),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int(fract_d1[j_loop,i_loop]*2**Q),width=Q)
            else:
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**R)-1),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**Q)-1),width=Q) 
            decimal[j_loop,i_loop]=int(Q1[j_loop][i_loop],base=2)                  
    return decimal

def decimal_to_real(weights_bin,Q=11,R=4):
    #make sure weights_bin has 2 dimn with the second dim as 1. 
    row_size,column_size=weights_bin.shape
    Q1=[]
    for i_loop in range(0,row_size):
            Q1.append(np.binary_repr(weights_bin[i_loop,0],width=Q+R+1))
    d1=np.zeros([row_size,1])
    for j_loop in range(0,row_size):
        if Q1[j_loop][0]=="0":
            sign_bit=1
        else:
            sign_bit=-1
        decimal_value=0    
        for decimcal_loop in range(Q+R,0,-1):
           if Q1[j_loop][(Q+R)-decimcal_loop+1]=='1':
               decimal_value=decimal_value+2**(decimcal_loop-Q-1)
           else:
               decimal_value=decimal_value+0       
        d1[j_loop,0]=sign_bit*decimal_value      
    return d1


Model_para_inputs=sio.loadmat('Model_para_inputs.mat')
Layer1_weights=Model_para_inputs['Layer1_weights']
Layer1_bias=Model_para_inputs['Layer1_bias']

#For the demonstrating the use I am only quantizing weights and biases from only the first neuron



input_test=Model_para_inputs['image_test']
input_real = np.concatenate([input_test[0,:],np.array([1.0])])
input_test_decimal=np.reshape(real_to_decimal(binary_to_real(real_to_binary(input_real, Q_SET, R_SET), Q_SET, R_SET), Q_SET, R_SET),[-1])
#Note there is a 1 added at the end of the input to enable multiplication of the bias with an input of 1. 
input_file = open("inputs4.txt", "w")
a=np.where(input_test_decimal!=0)
input_non_zero=input_test_decimal[a]
np.savetxt(input_file, input_non_zero,newline='\n',fmt='%d')

input_file.close()



Neuron1_weights=Layer1_weights[:,0]

Neuron1_weights_quant=binary_to_real(real_to_binary(Neuron1_weights, Q_SET, R_SET), Q_SET, R_SET)

#checking the quantization error
Quantization_error=Neuron1_weights - Neuron1_weights_quant
pyplot.plot(Quantization_error)



Neuron1_bias=Layer1_bias[0]

Neuron1_bias_quant_decimal=real_to_decimal(binary_to_real(real_to_binary(Neuron1_bias, Q_SET, R_SET), Q_SET, R_SET), Q_SET, R_SET)


Neuron1_weights_quant_decimal=real_to_decimal(Neuron1_weights_quant, Q_SET, R_SET)
Neuron1_bias_quant_decimal=Neuron1_bias_quant_decimal[0]

parameters=np.concatenate([np.reshape(Neuron1_weights_quant_decimal,[-1]),Neuron1_bias_quant_decimal])


parameters_real = np.concatenate((Neuron1_weights,[Neuron1_bias[0]]))



parameters_nonzero=parameters[a]
weights_file = open("weights4.txt", "w")
np.savetxt(weights_file, parameters_nonzero,newline='\n',fmt='%d')

weights_file.close()

#Compute the expected output of the first neuron:
input_real_shaped = np.reshape(input_real, [-1])
real_output= np.sum(np.dot(parameters_real, input_real_shaped))


print("Real output: {}".format(real_output))

quant_decimal_output = real_to_decimal(np.array([real_output]), Q_SET, R_SET)

print("Quant decimal output: {}".format(quant_decimal_output[0][0]))        
    

with open("MAC_output4.txt","r") as mac_output_file:
    lines = mac_output_file.readlines()

    python_outputs_real = np.ndarray(shape=(len(a[0]),), dtype=float)
    python_outputs_decimal = np.ndarray(shape=(len(a[0]),), dtype=int)

    sum = 0
    idx = 0
    
    for i in a[0]:
        sum += parameters_real[i] * input_real_shaped[i]
        
        python_outputs_real[idx] = sum
        #print("Real output for {}: {}".format(c, sum))
        decimal_sum = real_to_decimal(np.array([sum]), Q_SET, R_SET)
        #print("Decimal output for {}: {}".format(c, decimal_sum[0][0]))
        
        python_outputs_decimal[idx] = int(decimal_sum)
        
        idx += 1
        
    mac_outputs_decimal = np.ndarray(shape=(len(a[0]),1), dtype=int)
        
    for i in range(len(a[0])):
        #skip the first output of the mac unit because it is an extra zero
        
        mac_outputs_decimal[i][0] = int(lines[i + 1])
        
    mac_outputs_real = np.reshape(decimal_to_real(mac_outputs_decimal, Q_SET, R_SET),[-1])

    
    mac_quant_error = python_outputs_real - mac_outputs_real
    
    pyplot.plot(mac_quant_error)
    
    decimal_mac_result = int(lines[-1].strip())
    
    print("Quant decimal output from MAC unit: {}".format(decimal_mac_result))
    
    real_mac_result = decimal_to_real(np.array([[int(decimal_mac_result)],[0]]), Q_SET, R_SET)[0][0]
    
    print("Real output from MAC unit: {}".format(real_mac_result))    
    
    
    percent_error = 100*abs((real_output - real_mac_result)/real_output)
    
    print("MAC quant error:{}".format(real_output - real_mac_result))
    print("MAC percent error: {}%".format(percent_error))
    





#Checking the quantized data:
#pysim_test = np.sum(np.dot(parameters_nonzero, input_non_zero))
#
#pysim_test_arry = np.array([[int(pysim_test)],[0]])
#
#print(decimal_to_real(pysim_test_arry))



