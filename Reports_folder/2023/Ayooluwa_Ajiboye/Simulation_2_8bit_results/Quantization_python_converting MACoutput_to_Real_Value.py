# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 16:50:51 2021

@author: sshah389
"""

#This file takes in the real value and quantize and creates a text file to input the values into cadence.
#Here the code is doing fixed point quantization


import scipy.io as sio
import numpy as np
import matplotlib.pyplot as pyplot

Int = 3
Dec = 4

def real_to_binary(weights,Q=Dec,R=Int):
    d1=weights
    dim_array=d1.ndim
    if dim_array>1:
        [row_size,column_size]=d1.shape
    else:
        row_size=d1.shape
        d1=d1.reshape([row_size[0],1])
        [row_size,column_size]=d1.shape
    Q1=[["0"]]
    for j_loop in range(0,row_size-1):
        Q1.append(["0"])
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size-1):
                Q1[j_loop].append("0")
    integ_d1=np.floor(abs(d1))
    fract_d1=abs(d1)-integ_d1
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if d1[j_loop,i_loop]<0:
                Q1[j_loop][i_loop]='1'
            if d1[j_loop,i_loop]<=(2**R-1):
                Q1[j_loop][i_loop]+=np.binary_repr(int(integ_d1[j_loop,i_loop]),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int(fract_d1[j_loop,i_loop]*2**Q),width=Q)
            else:
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**R)-1),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**Q)-1),width=Q)              
    return Q1
            
                


def binary_to_real(weights_bin,Q=Dec,R=Int):
    Q1=weights_bin
    row_size=len(Q1)
    column_size=len(Q1[0])
    if column_size>1:
        d1=np.zeros([row_size,column_size])
    else:
        d1=np.zeros([row_size,1])
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if Q1[j_loop][i_loop][0]=="0":
                sign_bit=1
            else:
                sign_bit=-1
            decimal_value=0    
            for decimcal_loop in range(Q+R,0,-1):
               if Q1[j_loop][i_loop][(Q+R)-decimcal_loop+1]=='1':
                   decimal_value=decimal_value+2**(decimcal_loop-Q-1)
               else:
                   decimal_value=decimal_value+0       
            d1[j_loop,i_loop]=sign_bit*decimal_value
    if column_size==1:
        d1=d1.reshape([row_size])        
    return d1

    
def real_to_decimal(weights,Q=Dec,R=Int):
    d1=weights
    dim_array=d1.ndim
    if dim_array==1:
        row_size=d1.shape
        d1=d1.reshape([row_size[0],1])
    [row_size,column_size]=d1.shape
    decimal=np.zeros([row_size,column_size])
    Q1=[['0' for i in range(column_size)] for j in range(row_size)]
    integ_d1=np.floor(abs(d1))
    fract_d1=abs(d1)-integ_d1
    for j_loop in range(0,row_size):
        for i_loop in range(0,column_size):
            if d1[j_loop,i_loop]<0:
                Q1[j_loop][i_loop]='1'
            if d1[j_loop,i_loop]<=(2**R-1):
                Q1[j_loop][i_loop]+=np.binary_repr(int(integ_d1[j_loop,i_loop]),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int(fract_d1[j_loop,i_loop]*2**Q),width=Q)
            else:
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**R)-1),width=R)
                Q1[j_loop][i_loop]+=np.binary_repr(int((2**Q)-1),width=Q) 
            decimal[j_loop,i_loop]=int(Q1[j_loop][i_loop],base=2)                  
    return decimal

def decimal_to_real(weights_bin,Q=Dec,R=Int):
    #make sure weights_bin has 2 dimn with the second dim as 1. 
    row_size,column_size=weights_bin.shape
    Q1=[]
    for i_loop in range(0,row_size):
            Q1.append(np.binary_repr(weights_bin[i_loop,0],width=Q+R+1))
    d1=np.zeros([row_size,1])
    for j_loop in range(0,row_size):
        if Q1[j_loop][0]=="0":
            sign_bit=1
        else:
            sign_bit=-1
        decimal_value=0    
        for decimcal_loop in range(Q+R,0,-1):
           if Q1[j_loop][(Q+R)-decimcal_loop+1]=='1':
               decimal_value=decimal_value+2**(decimcal_loop-Q-1)
           else:
               decimal_value=decimal_value+0       
        d1[j_loop,0]=sign_bit*decimal_value      
    return d1


Model_para_inputs=sio.loadmat('Model_para_inputs.mat')
Layer1_weights=Model_para_inputs['Layer1_weights']
Layer1_bias=Model_para_inputs['Layer1_bias']

#For the demonstrating the use I am only quantizing weights and biases from only the first neuron



input_test=Model_para_inputs['image_test']
input_test_decimal=np.reshape(real_to_decimal(binary_to_real(real_to_binary(np.concatenate([input_test[0,:],np.array([1.0])])))),[-1])
#Note there is a 1 added at the end of the input to enable multiplication of the bias with an input of 1. 
input_file = open("inputs.txt", "w")
a=np.where(input_test_decimal!=0)
input_non_zero=input_test_decimal[a]
np.savetxt(input_file, input_non_zero,newline='\n',fmt='%d')

input_file.close()



Neuron1_weights=Layer1_weights[:,0]

Neuron1_weights_quant=binary_to_real(real_to_binary(Neuron1_weights))

#checking the quantization error
Quantization_error=Neuron1_weights - Neuron1_weights_quant
pyplot.plot(Quantization_error)



Neuron1_bias=Layer1_bias[0]

Neuron1_bias_quant_decimal=real_to_decimal(binary_to_real(real_to_binary(Neuron1_bias)))


Neuron1_weights_quant_decimal=real_to_decimal(Neuron1_weights_quant)
Neuron1_bias_quant_decimal=Neuron1_bias_quant_decimal[0]

parameters=np.concatenate([np.reshape(Neuron1_weights_quant_decimal,[-1]),Neuron1_bias_quant_decimal])


def readFile(fileName):
        fileObj = open(fileName, "r") #opens the file in read mode
        words = fileObj.read().splitlines() #puts the file into an array
        fileObj.close()
        return words
    
    
parameters_nonzero=parameters[a]
# out1 = input_non_zero*parameters_nonzero
# out1=np.array(out1,dtype=int)
# out1=np.reshape(out1,[117,1])
# Convert1=(decimal_to_real(out1, Q=11, R=4))
weights_file = open("weights.txt", "w")
np.savetxt(weights_file, parameters_nonzero,newline='\n',fmt='%d')

weights_file.close()

    
Neuron1_MAC=readFile("MAC_Output.txt")
Neuron1_MAC1=(np.genfromtxt('MAC_Output.txt', unpack=True))
Neuron1_MAC1=np.array(Neuron1_MAC1,dtype=int)
Neuron1_MAC1=np.reshape(Neuron1_MAC1,[112,1])
Convert=(decimal_to_real(Neuron1_MAC1, Q=Dec, R=Int))

input_txt=readFile("inputs.txt")
input_txt=(np.genfromtxt('inputs.txt', unpack=True))
input_txt=np.array(input_txt,dtype=int)
input_txt=np.reshape(input_txt,[112,1])

weights_txt=readFile("weights.txt")
weights_txt=(np.genfromtxt('weights.txt', unpack=True))
weights_txt=np.array(Neuron1_MAC1,dtype=int)
weights_txt=np.reshape(Neuron1_MAC1,[112,1])

out1 = decimal_to_real(input_txt)*decimal_to_real(weights_txt)









