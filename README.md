Please read Assignment_2.pdf
Note these tutorial are just an introduction to these tools.

The assignment involves characterizing MAC units with different bit precisions. The input and weights to the MAC units are dervied from a Neural Network trained using tensorflow with MNIST dataset. 

Cadence Tutorial:
https://www.brown.edu/Departments/Engineering/Courses/engn1600/Assignments/Cadence_Tutorial_EN1600.pdf

Design vision Tutorial:
https://classes.engineering.wustl.edu/ese461/Tutorial/design_compiler_tutorial.pdf


Verilog-A tutorial: https://verilogams.com/tutorials/vloga-intro.html
Verilog-A manual: http://www2.ece.ohio-state.edu/~bibyk/ece822/verilogamsref.pdf

